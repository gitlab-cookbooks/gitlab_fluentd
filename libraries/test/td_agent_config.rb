# frozen_string_literal: true

module TdAgentConfig
  def load_config(path)
    content = inspec.file(path).content

    if content.to_s.empty?
      fail_resource("Unable to read current #{resource_config[:config_path]}")
    end

    content
  end

  def write_config(path, new_config)
    # Escaping the right things ourselves, and avoid inspec double escaping single
    # quotes using shellwords resulting them not to be rendered in the config
    escaped_config = new_config.gsub("'", %q(\\\'))
    run("echo $'#{escaped_config}' > '#{path}'")
    written_config = inspec.file(path).content

    unless written_config.include?(new_config)
      fail_resource('Failed to create test config')
    end

    restart_td_agent
  end

  def restart_td_agent
    run('systemctl restart td-agent')
    # Wait until td-agent is running
    unless wait_until { inspec.service('td-agent').running? }
      fail_resource('td-agent failed to restart')
    end
  end
end
