# frozen_string_literal: true

module RunsCommand
  def run(command)
    result = inspec.bash(command)

    fail_resource("failed to run #{command}") if result.exit_status != 0

    result
  end

  def wait_until
    deadline = Time.now + 60

    result = yield until result || deadline < Time.now

    result
  end
end
