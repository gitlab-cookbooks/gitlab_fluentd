name             'gitlab_fluentd'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'FluentD log agent cookbook'
version          '1.57.1'
chef_version     '>= 12.1'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_fluentd/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_fluentd'

supports 'ubuntu', '= 16.04'

depends 'gitlab_secrets'
depends 'logrotate', '~> 2.2.0'
depends 'managed_directory'
