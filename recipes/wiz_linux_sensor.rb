# Cookbook:: gitlab_fluentd
# Recipe:: wiz_linux_sensor
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'wiz_linux_sensor.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[fluentd]', :delayed
end
