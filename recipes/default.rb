# Cookbook:: gitlab_fluentd
# Recipe:: default
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

# Fetch secrets from gitlab_server

if node['gitlab-server']['rsyslog_client'].attribute?('secrets')
  elastic_cloud_conf = get_secrets(node['gitlab-server']['rsyslog_client']['secrets']['backend'],
                                   node['gitlab-server']['rsyslog_client']['secrets']['path'])

  node.default['gitlab_fluentd']['pubsub_key'] = elastic_cloud_conf['pubsub_key']
end

apt_repository 'fluentd' do
  uri           node['gitlab_fluentd']['apt_repo_uri']
  distribution  node['gitlab_fluentd']['apt_repo_distribution']
  components    node['gitlab_fluentd']['apt_repo_components']
  key           node['gitlab_fluentd']['apt_repo_key']
end

apt_package 'build-essential'

# Stop old td-agent and clean up obsolete systemd unit.
execute 'stop old td-agent' do
  command 'systemctl stop td-agent.service'
  only_if 'systemctl is-active --quiet td-agent.service'
end

file '/etc/systemd/system/td-agent.service' do
  action :delete
  notifies :run, 'execute[systemd reload]', :immediately
  notifies :restart, 'service[fluentd]', :delayed
end

directory '/etc/systemd/system/fluentd.service.d' do
  mode '0755'
  recursive true
end

template '/etc/systemd/system/fluentd.service.d/override.conf' do
  source 'systemd-override.conf.erb'
  mode '0644'
  notifies :run, 'execute[systemd reload]', :immediately
  notifies :restart, 'service[fluentd]', :delayed
end

directory '/etc/fluent' do
  mode '0755'
  owner 'root'
  group 'root'
end

template '/etc/fluent/fluentd.environment' do
  source 'fluentd.environment.erb'
  owner 'root'
  group 'root'
  mode '0600'
  bind_secrets_helpers # Injects helpers for secrets from OmnibusSecrets
  notifies :restart, 'service[fluentd]', :delayed
end

apt_package 'fluent-package' do
  version node['gitlab_fluentd']['td-agent']['version']
  notifies :run, 'execute[systemd reload]', :immediately
end

execute 'systemd reload' do
  command 'systemctl daemon-reload'
  action :nothing
end

directory node['gitlab_fluentd']['config_dir_modules'] do
  mode '0755'
  owner 'root'
  group 'root'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

managed_directory node['gitlab_fluentd']['config_dir_modules'] do
  action :clean
  notifies :restart, 'service[fluentd]', :delayed
end

directory node['gitlab_fluentd']['es_template_dir'] do
  mode '0755'
  owner 'root'
  group 'root'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

directory node['gitlab_fluentd']['log_dir'] do
  mode '0755'
  owner '_fluentd'
  group '_fluentd'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

# td-agent v3 and v4 set the owner of /etc/td-agent to user td-agent.
# For some reason, some nodes have changed the owner to root. We need
# this to be owned by _fluentd so the bundle install can write to
# /etc/fluent/.bundle.
directory '/etc/fluent' do
  mode '0755'
  owner '_fluentd'
  group '_fluentd'
end

directory '/var/lib/fluent/vendor/bundle' do
  mode '0755'
  recursive true
  owner '_fluentd'
  group '_fluentd'
end

template '/etc/fluent/fluentd.conf' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[fluentd]', :delayed
end

# This is needed to allow FLUENT_PACKAGE_OPTIONS to be defined in override.conf
file '/etc/default/fluentd' do
  action :delete
  notifies :restart, 'service[fluentd]', :delayed
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'sessions.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[fluentd]', :delayed
end

file node['gitlab_fluentd']['pubsub_file'] do
  owner 'root'
  group 'root'
  mode '0600'
  content node['gitlab_fluentd']['pubsub_key']
  notifies :restart, 'service[fluentd]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_key'].nil? }
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
end

file node['gitlab_fluentd']['pubsub_file'] do
  action :delete
  notifies :restart, 'service[fluentd]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_enable'] }
end

cookbook_file '/etc/fluent/plugin/out_cloud_pubsub.rb' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[fluentd]', :delayed
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
end

cookbook_file '/etc/fluent/prometheus-mixin.conf' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[fluentd]', :delayed
end

file '/etc/fluent/plugin/out_cloud_pubsub.rb' do
  action :delete
  notifies :restart, 'service[fluentd]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_enable'] }
end

# old plugin name, deprecated but we ensure it is
# deleted if it still exists.
file '/etc/fluent/plugin/plugin-cloud-pubsub.rb' do
  action :delete
  notifies :restart, 'service[fluentd]', :delayed
end

## Install Gems

# The pg gem, used by fluent-plugin-postgresql-csvlog requires `pg_config`
# which is part of libpq-dev
# So install this here instead of in the posgres recipe to make sure this dependency
# is installed before we try to install the gems.
apt_package 'libpq-dev' do
  action :install
  only_if { node['gitlab_fluentd']['enable_postgres_csvlog_ingestion'] }
end

cookbook_file node['gitlab_fluentd']['gemfile'] do
  owner 'root'
  group 'root'
  mode '0644'
end

cookbook_file "#{node['gitlab_fluentd']['gemfile']}.lock" do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :run, 'execute[install fluentd gems]', :immediately
end

# td-agent will run bundle install at startup, but this may take a while
# and may time out systemd.
execute 'install fluentd gems' do
  bundle_groups_for_recipes = node['recipes'].map { |r| r.split('::').last }.join(':')
  bundle_install = "/opt/fluent/bin/bundle install --jobs 1 --verbose --deployment --gemfile=#{node['gitlab_fluentd']['gemfile']} --path=#{node['gitlab_fluentd']['gem_path']}"
  bundle_install = [bundle_install, "--with=#{bundle_groups_for_recipes}"].join(' ')

  command bundle_install
  # When BUNDLER_VERSION is set, bundle won't attempt to upgrade to the
  # version specified in Gemfile.lock, which would require root permissions
  # to install files in /opt/fluent/lib/ruby.
  environment({ 'BUNDLER_VERSION' => '2.4.19', 'IPV4_FALLBACK_ENABLED' => '1' })
  user '_fluentd'
  group '_fluentd'
  action :nothing
  notifies :restart, 'service[fluentd]', :delayed
end

# fluent is started in its package's postinst.
service 'fluentd' do
  action :enable
end

logrotate_app 'fluentd' do
  path '/var/log/fluent/fluentd.log'
  options %w(missingok compress delaycompress notifempty)
  rotate 7
  frequency 'daily'
  size '1G'
  postrotate <<-EOF
pid=/var/run/fluent/fluentd.pid
    if [ -s "$pid" ]
    then
      kill -USR1 "$(cat $pid)"
    fi
  EOF
end
