# Cookbook:: gitlab_fluentd
# Recipe:: pages
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'pages.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[fluentd]', :delayed
end
