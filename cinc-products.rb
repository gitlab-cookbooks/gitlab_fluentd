# This file is used to seed available products to test-kitchen
product 'cinc' do
  product_name 'Cinc Infra Client'
  package_name 'cinc-client'
  api_url 'https://packages.cinc.sh'
end
