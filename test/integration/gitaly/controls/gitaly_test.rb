# frozen_string_literal: true

# InSpec tests for recipe gitlab_fluentd::gitaly

# FIXME: pass the test!
# control 'gitaly tails' do
#   impact 1.0
#   title 'test for the collected output of gitaly logs'

#   describe gitaly_tails do
#     it { should exist }
#     its(:gitaly_log_entry) { should_not be_empty }
#     its(:gitaly_log_entry) { should_not include('unmatched_line') }
#     its(:gitaly_lfs_smudge_log_entry) { should_not be_empty }
#     its(:gitaly_lfs_smudge_log_entry) { should_not include('unmatched_line') }
#     it { should be_cleanup }
#   end
# end

control 'postgres development packages not installed' do
  impact 1.0
  title 'Checks that the packages from the postgres recipe are not installed'

  describe package('libpq-dev') do
    it { should_not be_installed }
  end
end

control 'td-agent bundle' do
  impact 1.0
  title 'Checks that only requried gems are installed'

  describe td_agent_bundle do
    it { should be_installed }
    it { should_not be_including_gem('fluent-plugin-postgresql-csvlog') }
  end
end
