# frozen_string_literal: true

require_relative 'test/test_helper'

class GitalyTails < Inspec.resource(1)
  include RunsCommand
  include RedirectsOutput
  include TdAgentConfig

  name 'gitaly_tails'

  # These attributes are populated on initalization and verified from the spec
  attr_reader :gitaly_log_entry, :gitaly_lfs_smudge_log_entry

  def initialize
    @original_config = load_config(resource_config[:config_path])

    redirect_output(resource_config[:config_path], resource_config[:log_path])

    run("mkdir -p #{resource_config[:gitaly_log_dir]}")

    @gitaly_log_entry = read_gitaly_log
    @gitaly_lfs_smudge_log_entry = read_gitaly_lfs_smudge_log
  end

  def exists?
    gitaly_log.exist?
  end

  def cleanup?
    write_config(resource_config[:config_path], original_config)

    if inspec.file(resource_config[:config_path]).content != original_config
      fail_resource('Failed to restore the config')
    end
  end

  private

  attr_reader :original_config

  def read_gitaly_log
    gitaly_log_file = File.join(resource_config[:gitaly_log_dir], 'current')

    read_new_log_line(resource_config[:log_path]) do
      time = Time.now.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      log_line = <<~LOG.strip
        {"type":"gitaly","grpc.start_time":"#{time}","grpc.time_ms":0.3,"grpc.code":"OK","grpc.method":"Check","grpc.service":"grpc.health.v1.Health","pid":13747,"correlation_id":"4fd8ecca229d31f62ab90e34a77fc4fa","time":"#{time}"}
      LOG

      run("echo '#{log_line}' >> '#{gitaly_log_file}'")
    end
  end

  def read_gitaly_lfs_smudge_log
    gitaly_lfs_smudge_log_file = File.join(resource_config[:gitaly_log_dir], 'gitaly_lfs_smudge.log')

    read_new_log_line(resource_config[:log_path]) do
      time = Time.now.utc.strftime('%Y-%m-%dT%H:%M:%S.%N%Z')
      log_line = <<~LOG.strip
        {"content_length_bytes":7352215,"correlation_id":"6sJIp3KuZG8","duration_ms":189,"level":"info","method":"GET","msg":"Finished HTTP request","status":200,"time":"#{time}","url":"http://unix/api/v4/internal/lfs?oid=46967a21e5d856eaea89d2e5dd55a5e3b5f4e1e4efe3b000ef6d60b31600f1d2\u0026gl_repository=project-200"}
      LOG

      run("echo '#{log_line}' >> '#{gitaly_lfs_smudge_log_file}'")
    end
  end

  def gitaly_log
    inspec.file(resource_config[:log_path])
  end

  def resource_config
    @resource_config ||= {
      config_path: '/etc/fluent/conf.d/gitaly.conf',
      gitaly_log_dir: '/var/log/gitlab/gitaly',
      log_path: '/tmp/inspec/gitaly.log',
    }
  end
end
