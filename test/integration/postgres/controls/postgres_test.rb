# frozen_string_literal: true

# InSpec tests for recipe gitlab_fluentd::postgres

control 'Gem fluent-plugin-postgresql-csvlog is installed' do
  impact 1.0
  title 'Checks that required packages are installed'

  describe td_agent_bundle do
    it { should be_including_gem('fluent-plugin-postgresql-csvlog') }
    it { should have_config('BUNDLE_WITH' => 'postgres:default') }
  end
end

control 'postgres development packages installed' do
  impact 1.0
  title 'Checks that required packages are installed'

  describe package('libpq-dev') do
    it { should be_installed }
  end
end

control 'postgres.conf' do
  impact 1.0
  title 'Checks postgres.conf is correctly configured'

  pg_stat_statements_source = <<~'INPUT'
    <source>
      @type pg_stat_statements
      tag postgres.pg_stat_statements
      host localhost
      port 5432
      username "#{ENV['FLUENTD_POSTGRES_INPUT_USERNAME']}"
      password "#{ENV['FLUENTD_POSTGRES_INPUT_PASSWORD']}"
      dbname gitlabhq_production
      sslmode prefer
      interval 1800
      # Google Cloud Logging limits messages to 256K: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/3248
      max_length 200000
    </source>
  INPUT

  pg_stat_activity_source = <<~'INPUT'
    <source>
      @type pg_stat_activity
      tag postgres.pg_stat_activity
      host localhost
      port 5432
      username "#{ENV['FLUENTD_POSTGRES_INPUT_USERNAME']}"
      password "#{ENV['FLUENTD_POSTGRES_INPUT_PASSWORD']}"
      dbname gitlabhq_production
      sslmode prefer
      interval 120
      # Google Cloud Logging limits messages to 256K: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/3248
      max_length 200000
    </source>
  INPUT

  describe file('/etc/fluent/conf.d/postgres.conf') do
    its('content') { should include pg_stat_statements_source }
  end

  describe file('/etc/fluent/conf.d/postgres.conf') do
    its('content') { should include pg_stat_activity_source }
  end

  describe file('/etc/fluent/fluentd.environment') do
    its('content') { should include 'FLUENTD_POSTGRES_INPUT_USERNAME=postgres_exporter' }
    its('content') { should include 'FLUENTD_POSTGRES_INPUT_PASSWORD=' }
    its('mode') { should cmp '0600' }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
  end
end
