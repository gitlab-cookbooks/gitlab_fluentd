# frozen_string_literal: true

require_relative 'test/test_helper'

class SlowlogOutput < Inspec.resource(1)
  include RunsCommand
  include RedirectsOutput
  include TdAgentConfig

  name 'slowlog_output'

  def initialize(redis_password)
    @original_config = load_config(resource_config[:config_path])
    @redis_password = redis_password

    reconfigure_td_agent
    read_new_slowlog_entry
  end

  def exists?
    slowlog.exist?
  end

  def has_key?(key)
    last_slowlog_entry[key]
  end

  def exec_time_s
    last_slowlog_entry['exec_time_s']
  end

  def cleanup?
    write_config(resource_config[:config_path], original_config)

    if inspec.file(resource_config[:config_path]).content != original_config
      fail_resource('Failed to restore the config')
    end
  end

  private

  attr_reader :original_config, :redis_password
  attr_accessor :last_slowlog_entry

  def resource_config
    @resource_config ||= {
      config_path: '/etc/fluent/conf.d/redis.conf',
      slowlog_path: '/tmp/inspec/slowlog.log',
    }
  end

  def reconfigure_td_agent
    redirect_output(resource_config[:config_path], resource_config[:slowlog_path])
  end

  def slow_command
    # A slow eval
    "EVAL 'local a=0; while(a < 999999) do a = a+1 end' 0"
  end

  def run_slow_command
    run("echo \"#{slow_command}\" | redis-cli -x -a '#{redis_password}'")
  end

  def read_new_slowlog_entry
    new_entry = read_new_log_line(resource_config[:slowlog_path]) do
      run_slow_command
    end

    unless new_entry
      fail_resource('Failed to read new entry from the slowlog in time')
      return
    end

    self.last_slowlog_entry = JSON.parse(new_entry)
  rescue JSON::ParserError => e
    fail_resource("Could not parse slowlog: #{e.message}")
  end

  def slowlog
    inspec.file(resource_config[:slowlog_path])
  end
end
