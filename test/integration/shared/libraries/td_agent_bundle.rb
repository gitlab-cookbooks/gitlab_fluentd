require_relative 'test/test_helper'

class TdAgentBundle < Inspec.resource(1)
  include RunsCommand

  name 'td_agent_bundle'

  def initialize
    bundle_show = run(<<~CMD)
      cd /etc/fluent; /opt/fluent/bin/bundle show
    CMD
    @gem_list = bundle_show.stdout
    @bundle_check = run('/opt/fluent/bin/bundle check --gemfile /etc/fluent/Gemfile --path /var/lib/fluent/vendor/bundle')
    @bundle_config = YAML.safe_load(inspec.file('/etc/fluent/.bundle/config').content)
  end

  def including_gem?(name)
    @gem_list.include?(name)
  end

  def installed?
    @bundle_check.exit_status == 0
  end

  def has_config?(expected_config)
    @bundle_config.values_at(*expected_config.keys) == expected_config.values
  end
end
