describe 'gitlab_fluentd::haproxy' do
  before do
    stub_command('systemctl is-active --quiet td-agent.service').and_return(5)
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '18.04') do |node|
        node.normal['prometheus']['labels'] = {
          'stage' => 'main',
          'shard' => 'default',
          'type' => 'lb',
        }
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to_not include 'enable_monitoring true'
      }
    end

    it 'sets the buffer_chunk_limit' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'buffer_chunk_limit 4MB'
      }
    end

    it 'sets the buffer_queue_limit' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'buffer_queue_limit 128'
      }
    end

    it 'sets the flush_interval' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'flush_interval 30s'
      }
    end

    it 'sets the log_level' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'log_level info'
      }
    end

    it 'renders the haproxy config correctly' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/haproxy.template'))
      }
    end
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '18.04') do |node|
        node.normal['gitlab_fluentd']['haproxy']['buffer_chunk_limit'] = '8MB'
        node.normal['gitlab_fluentd']['haproxy']['buffer_queue_limit'] = '64'
        node.normal['gitlab_fluentd']['haproxy']['flush_interval'] = '60s'
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'sets the buffer_chunk_limit' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'buffer_chunk_limit 8MB'
        expect(content).to include 'buffer_queue_limit 64'
        expect(content).to include 'flush_interval 60s'
        expect(content).to include 'log_level info'
      }
    end
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '18.04') do |node|
        node.normal['gitlab_fluentd']['google_cloud_monitoring'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'enable_monitoring true'
      }
    end
  end

  context 'when use_file_buffer is true' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '18.04') do |node|
        node.normal['gitlab_fluentd']['haproxy']['use_file_buffer'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'configures a file buffer' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'buffer_type file'
        expect(content).to include 'buffer_path /opt/fluent/buffers/haproxy'
      }
    end
  end
end
