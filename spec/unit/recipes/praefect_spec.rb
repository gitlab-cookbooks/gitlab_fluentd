require 'spec_helper'

describe 'gitlab_fluentd::praefect' do
  before do
    stub_command('systemctl is-active --quiet td-agent.service').and_return(5)
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '18.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_env'] = 'prd'
        node.normal['gitlab_fluentd']['pubsub_project'] = 'production'
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'renders the praefect config correctly' do
      expect(chef_run).to(render_file('/etc/fluent/conf.d/praefect.conf').with_content do |content|
        expect(content).to eq(IO.read('spec/fixtures/praefect.template'))
      end)
    end
  end
end
