require 'spec_helper'

describe 'gitlab_fluentd::postgres' do
  before do
    stub_command('systemctl is-active --quiet td-agent.service').and_return(5)
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '18.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
            },
          },
        }
        node.normal['gitlab_fluentd']['wale_enabled'] = true
        node.normal['gitlab_fluentd']['repmgrd_enabled'] = false
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_env'] = 'prd'
        node.normal['gitlab_fluentd']['pubsub_project'] = 'production'
      end.converge(described_recipe)
    end

    it 'deletes the multiline CSV plugin' do
      expect(chef_run).to delete_file('/etc/fluent/plugin/parser_multiline_csv.rb')
    end

    it 'does not install libpq-dev' do
      expect(chef_run).not_to install_package('libpq-dev')
    end

    it 'deletes the PostgreSQL slow log plugin' do
      expect(chef_run).to delete_file('/etc/fluent/plugin/filter_postgresql_slowlog.rb')
    end

    it 'renders the postgres config correctly' do
      expect(chef_run).to render_file('/etc/fluent/conf.d/postgres.conf').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/postgres.template'))
      }
    end
  end

  context 'parsing vacuum log lines' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'backend' => 'fake_backend',
            'path' => 'fake_path',
          },
        }
      end.converge(described_recipe)
    end

    def parse_fields(example, regex)
      captures = example.match(find_expression_in_content(regex)).named_captures

      # symbolize keys
      captures.transform_keys(&:to_sym)
    end

    def find_expression_in_content(regex)
      expect(chef_run).to(render_file('/etc/fluent/conf.d/postgres.conf').with_content do |content|
        return Regexp.new(content.scan(Regexp.new(regex)).flatten.first)
      end)
    end

    let(:find_vacuum_regex) do
      <<~REGEX
        <filter postgres.postgres_csv>
          @type parser
          key_name message
          inject_key_prefix auto_vacuum_      # Enriched fields will use this prefix
          remove_key_name_field false         # Keep original message
          reserve_data true                   # Keep original fields
          reserve_time true                   # Keep original time
          emit_invalid_record_to_error false  # Ignore fields that don't match the parser regexp

          <parse>
            @type regexp
            types index_scans:integer,pages_removed:integer,pages_remain:integer,pages_skipped_pins:integer,pages_skipped_frozen:integer,tuples_removed:integer,tuples_remain:integer,tuples_dead_not_removable:integer,buffers_hits:integer,buffers_misses:integer,buffers_dirtied:integer,read_rate_avg_mbps:float,write_rate_avg_mbps:float,cpu_user_s:float,cpu_system_s:float,elapsed_s:float

            # parse vacuum log lines
            expression /(.+)/
          </parse>
        </filter>
      REGEX
    end

    it 'extracts all fields from aggressive vacuum log lines' do
      data = parse_fields(<<~EXAMPLE, find_vacuum_regex)
        automatic aggressive vacuum of table "gitlabhq_production.public.draft_notes": index scans: 1
        pages: 0 removed, 28066 remain, 0 skipped due to pins, 25122 skipped frozen
        tuples: 812 removed, 198065 remain, 12 are dead but not yet removable, oldest xmin: 1075421965
        buffer usage: 4484 hits, 42141 misses, 1368 dirtied
        avg read rate: 135.962 MB/s, avg write rate: 4.414 MB/s
        system usage: CPU: user: 0.25 s, system: 0.19 s, elapsed: 2.42 s
      EXAMPLE

      expect(data).to eq(
        aggressive: 'aggressive',
        vacuum: 'vacuum',
        wraparound: nil,
        database: 'gitlabhq_production',
        schema: 'public',
        tablename: 'draft_notes',
        index_scans: '1',
        pages_removed: '0',
        pages_remain: '28066',
        pages_skipped_pins: '0',
        pages_skipped_frozen: '25122',
        tuples_removed: '812',
        tuples_remain: '198065',
        tuples_dead_not_removable: '12',
        oldest_xmin: '1075421965',
        buffers_hits: '4484',
        buffers_misses: '42141',
        buffers_dirtied: '1368',
        read_rate_avg_mbps: '135.962',
        write_rate_avg_mbps: '4.414',
        cpu_user_s: '0.25',
        cpu_system_s: '0.19',
        elapsed_s: '2.42'
      )
    end

    it 'extracts all fields from vacuum log lines in "wraparound protection" mode' do
      data = parse_fields(<<~EXAMPLE, find_vacuum_regex)
        automatic aggressive vacuum to prevent wraparound of table "gitlabhq_production.pg_toast.pg_toast_559976213": index scans: 0
        pages: 0 removed, 0 remain, 0 skipped due to pins, 0 skipped frozen
        tuples: 0 removed, 0 remain, 0 are dead but not yet removable, oldest xmin: 1075895098
        buffer usage: 25 hits, 1 misses, 1 dirtied
        avg read rate: 5.923 MB/s, avg write rate: 5.923 MB/s
        system usage: CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.00 s
      EXAMPLE

      expect(data).to eq(
        aggressive: 'aggressive',
        vacuum: 'vacuum',
        wraparound: 'to prevent wraparound',
        database: 'gitlabhq_production',
        schema: 'pg_toast',
        tablename: 'pg_toast_559976213',
        index_scans: '0',
        pages_removed: '0',
        pages_remain: '0',
        pages_skipped_pins: '0',
        pages_skipped_frozen: '0',
        tuples_removed: '0',
        tuples_remain: '0',
        tuples_dead_not_removable: '0',
        oldest_xmin: '1075895098',
        buffers_hits: '25',
        buffers_misses: '1',
        buffers_dirtied: '1',
        read_rate_avg_mbps: '5.923',
        write_rate_avg_mbps: '5.923',
        cpu_user_s: '0.00',
        cpu_system_s: '0.00',
        elapsed_s: '0.00'
      )
    end

    it 'extracts all fields from vacuum log lines' do
      data = parse_fields(<<~EXAMPLE, find_vacuum_regex)
        automatic vacuum of table "gitlabhq_production.pg_toast.pg_toast_2619": index scans: 1
        pages: 0 removed, 13109 remain, 0 skipped due to pins, 8554 skipped frozen
        tuples: 21 removed, 45467 remain, 275 are dead but not yet removable, oldest xmin: 1077011482
        buffer usage: 4238 hits, 7 misses, 3 dirtied
        avg read rate: 3.455 MB/s, avg write rate: 1.481 MB/s
        system usage: CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.01 s
      EXAMPLE

      expect(data).to eq(
        aggressive: nil,
        vacuum: 'vacuum',
        wraparound: nil,
        database: 'gitlabhq_production',
        schema: 'pg_toast',
        tablename: 'pg_toast_2619',
        index_scans: '1',
        pages_removed: '0',
        pages_remain: '13109',
        pages_skipped_pins: '0',
        pages_skipped_frozen: '8554',
        tuples_removed: '21',
        tuples_remain: '45467',
        tuples_dead_not_removable: '275',
        oldest_xmin: '1077011482',
        buffers_hits: '4238',
        buffers_misses: '7',
        buffers_dirtied: '3',
        read_rate_avg_mbps: '3.455',
        write_rate_avg_mbps: '1.481',
        cpu_user_s: '0.00',
        cpu_system_s: '0.00',
        elapsed_s: '0.01'
      )
    end
  end

  context 'with pg_stat_statements enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'backend' => 'fake_backend',
            'path' => 'fake_path',
          },
        }
        node.normal['gitlab_fluentd']['postgres_pg_stat_statements_enable'] = true
      end.converge(described_recipe)
    end

    before do
      allow_any_instance_of(OmnibusSecrets)
        .to receive(:postgres_exporter_password)
        .and_return('5ecr3t')
    end

    it 'renders the pg_stat_statements input in the config' do
      pg_stat_statements_source = <<~'INPUT'
        <source>
          @type pg_stat_statements
          tag postgres.pg_stat_statements
          host localhost
          port 5432
          username "#{ENV['FLUENTD_POSTGRES_INPUT_USERNAME']}"
          password "#{ENV['FLUENTD_POSTGRES_INPUT_PASSWORD']}"
          dbname gitlabhq_production
          sslmode prefer
          interval 1800
          # Google Cloud Logging limits messages to 256K: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/3248
          max_length 200000
        </source>
      INPUT

      td_agent_environment = <<~'INPUT'
        FLUENTD_POSTGRES_INPUT_USERNAME=postgres_exporter
        FLUENTD_POSTGRES_INPUT_PASSWORD=5ecr3t
      INPUT

      expect(chef_run).to(render_file('/etc/fluent/conf.d/postgres.conf').with_content do |content|
        expect(content).to include(pg_stat_statements_source)
      end)

      expect(chef_run).to(render_file('/etc/fluent/fluentd.environment').with_content do |content|
        expect(content).to include(td_agent_environment)
      end)
    end
  end

  context 'with pg_stat_activity enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'backend' => 'fake_backend',
            'path' => 'fake_path',
          },
        }
        node.normal['gitlab_fluentd']['postgres_pg_stat_activity_enable'] = true
      end.converge(described_recipe)
    end

    before do
      allow_any_instance_of(OmnibusSecrets)
        .to receive(:postgres_exporter_password)
        .and_return('5ecr3t')
    end

    it 'renders the pg_stat_activity input in the config' do
      pg_stat_activity_source = <<~'INPUT'
        <source>
          @type pg_stat_activity
          tag postgres.pg_stat_activity
          host localhost
          port 5432
          username "#{ENV['FLUENTD_POSTGRES_INPUT_USERNAME']}"
          password "#{ENV['FLUENTD_POSTGRES_INPUT_PASSWORD']}"
          dbname gitlabhq_production
          sslmode prefer
          interval 120
          # Google Cloud Logging limits messages to 256K: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/3248
          max_length 200000
        </source>
      INPUT

      td_agent_environment = <<~'INPUT'
        FLUENTD_POSTGRES_INPUT_USERNAME=postgres_exporter
        FLUENTD_POSTGRES_INPUT_PASSWORD=5ecr3t
      INPUT

      expect(chef_run).to(render_file('/etc/fluent/conf.d/postgres.conf').with_content do |content|
        expect(content).to include(pg_stat_activity_source)
      end)

      expect(chef_run).to(render_file('/etc/fluent/fluentd.environment').with_content do |content|
        expect(content).to include(td_agent_environment)
      end)
    end
  end
end
