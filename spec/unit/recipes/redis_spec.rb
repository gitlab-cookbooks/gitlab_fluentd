require 'spec_helper'

describe 'gitlab_fluentd::redis' do
  platform 'ubuntu', '18.04'

  normal_attributes['gitlab_fluentd']['redis_logfile'] = '/var/log/redis/redis.log'
  normal_attributes['gitlab_fluentd']['stackdriver_enable'] = true
  normal_attributes['gitlab_fluentd']['pubsub_enable'] = true
  normal_attributes['gitlab_fluentd']['pubsub_env'] = 'prd'
  normal_attributes['gitlab_fluentd']['pubsub_project'] = 'production'
  normal_attributes['gitlab-server'] = {
    'rsyslog_client' => {
      'secrets' => {
        'backend' => 'fake_backend',
        'path' => 'fake_path',
      },
    },
  }

  before do
    stub_command('systemctl is-active --quiet td-agent.service').and_return(5)

    allow_any_instance_of(OmnibusSecrets)
      .to receive(:redis_password)
      .and_return('p@55w0rd!')
  end

  it 'renders the basic redis log output' do
    redis_input = <<~'INPUT'
      <source>
        @type tail
        tag redis
        path /var/log/redis/redis.log
        pos_file /var/log/fluent/redis.log.pos
        format /(?<redis_pidrole>\d+\:[MSXC]) (?<redis_message>(?<time>\d+ \S+ \d{4} \d+:\d+:\d+\.\d+) .*)/
        time_key time
        time_format %d %b %Y %H:%M:%S.%N
      </source>
    INPUT

    expect(chef_run).to(render_file('/etc/fluent/conf.d/redis.conf').with_content do |content|
      expect(content).to include(redis_input)
    end)
  end

  context 'with slowlog enabled' do
    normal_attributes['gitlab_fluentd']['redis_slowlog_enabled'] = true

    it 'renders the slowlog input in the config' do
      slowlog_input = <<~'INPUT'
        <source>
          @type redis_slowlog
          tag redis.slowlog
          password "#{ENV['FLUENTD_REDIS_PASSWORD']}"
        </source>
      INPUT

      td_agent_environment = <<~INPUT
        FLUENTD_REDIS_PASSWORD=p@55w0rd!
      INPUT

      expect(chef_run).to(render_file('/etc/fluent/conf.d/redis.conf').with_content do |content|
        expect(content).to include(slowlog_input)
      end)

      expect(chef_run).to(render_file('/etc/fluent/fluentd.environment').with_content do |content|
        expect(content).to include(td_agent_environment)
      end)
    end
  end

  context 'with sentinel enabled' do
    normal_attributes['gitlab_fluentd']['redis_sentinel_enabled'] = true

    it 'renders the sentinel input in the config' do
      sentinel_input = <<~'INPUT'
        <source>
          @type tail
          tag redis.sentinel
          path /var/log/gitlab/sentinel/current
          pos_file /var/log/fluent/sentinel.log.pos
          format /(?<time>[^ ]*) (?<sentinel_pidrole>[^ ]*) (?<sentinel_message>.*)/
          time_format %Y-%m-%d_%H:%M:%S
        </source>
      INPUT

      expect(chef_run).to(render_file('/etc/fluent/conf.d/redis.conf').with_content do |content|
        expect(content).to include(sentinel_input)
      end)
    end
  end
end
